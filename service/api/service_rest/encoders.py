from .models import Technician,Appointment,AutomobileVO
from common.json import ModelEncoder

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties=[
        "href",
        "vin",
        "sold"
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id"
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties= [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "is_vip"
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }
    def get_extra_data(self, o):
        return {"is_vip": o.is_vip()}
