from django.contrib import admin
from .models import Technician, AutomobileVO, Appointment

# Register your models here.
admin.site.register(Appointment)
admin.site.register(Technician)
admin.site.register(AutomobileVO)
