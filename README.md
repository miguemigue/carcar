# CarCar

This is a design and planning document for auto service and sales center called CarCar. Two microservices, Service microservice and Sales microservice, are built in this project. Third Inventory Microservice is used for polling and building Frontends only. The Service microservice handles appointments, technicians, and automobiles, while the Sales microservice handles sales, manufacturers, and customers.
The project plan includes instructions for running the project, value objects, models for the Service microservice, and RESTful APIs for both microservices.

# Team

### Person 1 - Kiran Hundal - Service
     - Service Microservice(fullStack)
     - Inventory Microservice - FrontEnd
            * Show a list of manufacturers
            * Create a manufacturer
            * Show a list of vehicle models

### Person 2 - Miguel Ortiz - Sales
    - Sales Microservice(fullStack)
     - Inventory Microservice - FrontEnd
            * Create a vehicle model
            * Show a list of automobiles in inventory
            * Create an automobile in inventory

# Design

![Design](Design.png)

# Running the Project

### Requirements:

* Git: Version 2.40.0 or later.
* Docker: Version 24.0.2 or later.
* Gitlab Repository: [Project Beta](https://gitlab.com/hundalkiran6/project-beta)

### Instructions:
** Have the Docker Desktop ready and installed to run this project:  https://docs.docker.com/engine/install/

1. cd into the directory you want to clone this project into.
2. Clone the project repository by running the following command: `git clone https://gitlab.com/hundalkiran6/project-beta`
3. Navigate to the cloned repository directory by running the following command: `cd project-beta`
4. Create a Docker volume named beta-data for projects databse: `docker volume create beta-data`
5. Build the Docker image by running the following command: `docker-compose build`
6. Start the Docker containers by running the following command: `docker-compose up`
7. Once the containers are up and running, navigate to `http://localhost:3000`
8. Application should be up and running and you will see the home page with Nav bar.

### Creating a superuser:

To log into the Admin application for any microservice, you'll need to create a superuser:

1. Connect to a running container using the command `docker exec -it «api-container-name» bash` which opens the container's command prompt.

2. Use the command `service list` to list all the running services and choose the one you want to connect to.

3. Run `python manage.py createsuperuser` to creat a superuser.

4. Microservices you can connect to by creating superusers are:

    - Inventory: http://localhost:8100/admin/
    - Service: http://localhost:8080/admin/
    - Sales: http://localhost:8090/admin/


# Nav bar routes:

### Inventory Routes

- **Manufacturers** http://localhost:3000/manufacturers/
    -  A list view of all manufacturer objects
- **Create a manufacturer** http://localhost:3000/manufacturers/create/
    - A form for creating a new and unique manufacturer object
- **Models** http://localhost:3000/models/
    - A list view of all vehicle model objects
- **Create a Model** http://localhost:3000/models/create/
    - A form for creating a new vehicle model object
- **Automobiles** http://localhost:3000/automobiles/
    - A list of all automobile objects
- **Create an Automobile** http://localhost:3000/automobiles/create/
    - A form for creating a new automobile object


### Service Routes

- **Technicians** http://localhost:3000/technicians
    -  A list view of all technician in the database
- **Create a technician** http://localhost:3000/technicians/create
    - A form for creating a new and unique technician
- **Service Appointment** http://localhost:3000/appointments
    - A list view of all the service appointments
- **Create a service appointment** http://localhost:3000/appointments/create
    - A form for creating a new service appointments for autombiles
- **Service History** http://localhost:3000/appointments/history
    - A list view of all the service appointments which can be searched with the VIN number


### Sales Routes

- **Salespeople** http://localhost:3000/salespeople/
    -  A list view of all salesperson objects
- **Add a salesperson** http://localhost:3000/salespeople/create/
    - A form for creating a new salesperson object
- **Customers** http://localhost:3000/customers/
    - A list view of all customer objects
- **Add a customer** http://localhost:3000/customers/create/
    - A form for creating a new salesperson object
- **Sales** http://localhost:3000/sales/
    - A list view of all sale objects
- **Add a sale** http://localhost:3000/sales/create/
    - A form for creating a new sale object
- **Sales History** http://localhost:3000/sales/history/
    - A list view of all sale objects, which can be filtered by salesperson using the dropdown menu


# Inventory Microservice Models

This microservice has three models:

1. The `Manufacturer` model has a unique `name` field to store the name of the manufacturer of the vehicles.

2. The `VehicleModel` model has fields for `name`, `picture_url`, and a foreign key to the `Manufacturer` model.

3.  The `Automobile` model has fields for `color`, `year`, `vin`, `sold_status`, and a foreign key to the `VehicleModel` model. The `vin` field is unique to store the vehicle identification number for every automobile in the inventory.



# Inventory Microservice RESTful API

| Action                 | Method | URL   |
|------------------------|--------|----------------------------------------   |
| List all manufacturers | GET    | http://localhost:8100/api/manufacturers/  |
| Show a specific manufacturer    | GET  | http://localhost:8100/api/manufacturers/:id/  |
| Create a manufacturer  | POST   | http://localhost:8100/api/manufacturers/ |
| Delete a manufacturer  | DELETE | http://localhost:8100/api/manufacturers/:id//  |
| Update a manufacturer  | PUT    |http://localhost:8100/api/manufacturers/:id/  |

<details>
<summary markdown="span">GET (List): Returns an object with one key "manufacturers" set to a list of all Manufacturer objects.
</summary>
JSON return body:

```
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Ford"
		}
	]
}
```
</details>

<details>
<summary markdown="span">GET: Returns an object containing the href, id, and name of the requested Manufacturer object.
</summary>
JSON return body:

```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Ford"
}
```
</details>

<details>
<summary markdown="span">POST: Creating a manufacturer requires a name field. Returns all of the values from the request body, plus the id of the created object.
</summary>
JSON request body:

```
{
  "name": "Ford"
}
```

JSON return body:

```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Ford"
}
```
</details>

<details>
<summary markdown="span">PUT: Updating a manufacturer requires an href, id, and name field. Returns all of the same fields from the request body.
</summary>
JSON request body:

```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Honda"
}
```

JSON return body:

```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Honda"
}
```
</details>

| Action  | Method   | URL   |
|---|---|---|
| List models | GET  | http://localhost:8100/api/models/  |
| Show a specific model | GET  |  http://localhost:8100/api/models/:id/  |
| Create a model | POST   | http://localhost:8100/api/models/ |
| Delete a model | DELETE  |  http://localhost:8100/api/models/:id/  |
| Update a model | PUT |  http://localhost:8100/api/models/:id/  |

<details>
<summary markdown="span">GET (List): Returns an object with one key "models" set to a list of all VehicleModel objects.
</summary>
JSON return body:

```
{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Sebring",
			"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Ford"
			}
		}
	]
}
```
</details>

<details>
<summary markdown="span">GET: Returns an object containing the href, id, name, picture url, and manufacturer of the requested VehicleModel object.
</summary>
JSON return body:

```
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Ford"
	}
}
```
</details>

<details>
<summary markdown="span">POST: Creating a model requires a name, picture url, and manufacturer id.
Returns all of the fields from the request body plus the id of the created model, with the manufacturer id replaced by the appropriate manufacturer object.
</summary>
JSON request body:

```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
```

JSON return body:

```
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Ford"
	}
}
```
</details>

<details>
<summary markdown="span">PUT: Updating a model requires either a name or picture url, but can take both. The manufacturer cannot be changed. Returns all of the same fields as the POST request.
</summary>
JSON request body:

```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}
```

JSON return body:

```
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Ford"
	}
}
```
</details>

| Action               | Method   | URL                                      |
|---|---|---|
| List all automobiles | GET      | http://localhost:8100/api/automobiles/  |
| Show an specific automobile     | GET  | http://localhost:8100/api/automobiles/:vin/  |
| Create an automobile  | POST    | http://localhost:8100/api/automobiles/ |
| Delete an automobile  | DELETE  |  http://localhost:8100/api/automobiles/:vin/  |
| Update an automobile  | PUT     |  http://localhost:8100/api/automobiles/:vin/  |

<details>
<summary markdown="span">GET (List): Returns an object with one key "autos" set to a list of all Automobile objects.
</summary>
JSON return body:

```
{
	"autos": [
		{
			"href": "/api/automobiles/1C3CC5FB2AN120174/",
			"id": 1,
			"color": "red",
			"year": 2012,
			"vin": "1C3CC5FB2AN120174",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Sebring",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Ford"
				}
			},
			"sold": false
		}
	]
}
```
</details>

<details>
<summary markdown="span">GET: Returns an object containing the href, id, color, year, vin, and model of the requested Automobile object.
</summary>
JSON return body:

```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "1C3CC5FB2AN120174",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Ford"
		}
	},
	"sold": false
}
```
</details>

<details>
<summary markdown="span">POST: Creating an automobile requires a color, year, vin, and model id.
Returns all of the fields from the request body plus the id and href of the created model, with the model id replaced by the appropriate VehicleModel object.
</summary>
JSON request body:

```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120175",
  "model_id": 1
}
```

JSON return body:

```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "1C3CC5FB2AN120175",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Ford"
		}
	},
	"sold": false
}
```
</details>

<details>
<summary markdown="span">PUT: Updating an automobile requires color, year, or sold. An The model and vin cannot be changed. Returns all of the same fields as the POST request.
</summary>
JSON request body:

```
{
  "color": "red",
  "year": 2012,
  "sold": true
}
```

JSON return body:

```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "1C3CC5FB2AN120174",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Ford"
		}
	},
	"sold": true
}
```
</details>


# Sales Microservice Models

The Sales microservice has three models:

1. The `Sale` model has fields for `automobile`, `salesperson`, `customer`, and `price`. Information for the appointments are stored with this model and it has **3** foreign keys to the `AutomobileVO`, `Salesperson`, and `Customer` models. These models have the capability to handle status changes for Salees (create, update and delete). The `vin` field stores the VIN number of every automobile in the inventory and also allows clients who did not buy the vehicle from the inventory to make appointments.

2. The `Salesperson` model contains fields for `first_name`, `last_name`, and `employee_id`, which is a primary key.

3. The `Customer` model contains fields for `first_name`, `last_name`, `address`, and `phone_number`, which is a primary key.

4. The `AutomobileVO` model is integrated with the **Inventory** microservice and has a unique `vin` field. A Poller is used to poll data from the `Automobile` model into the `AutomobileVO` model.


# Sales Microservice RESTful API

| Action  | Method   | URL   |
|---|---|---|
| List all salespeople | GET  | http://localhost:8090/api/salespeople/  |
| Show a specific salesperson  | GET  |  http://localhost:8090/api/salespeople/:id/  |
| Create a salesperson  | POST   | http://localhost:8090/api/salespeople/ |
| Delete a salesperson  | DELETE  |  http://localhost:8090/api/salespeople/:id/  |

<details>
<summary markdown="span">GET (List): Returns an object with one key "salespeople" set to a list of all Salesperson objects.
</summary>
JSON return body:

```
{
	"salespeople": [
		{
			"href": "/api/salespeople/1/",
			"id": 1,
			"first_name": "Thomas",
			"last_name": "Thompson",
			"employee_id": "TT123"
		},
		{
			"href": "/api/salespeople/2/",
			"id": 2,
			"first_name": "Charlie",
			"last_name": "Brown",
			"employee_id": "CB123"
		}
	]
}
```
</details>

<details>
<summary markdown="span">GET: Returns an object with one key "salesperson" set to the requested Salesperson object.
</summary>
JSON return body:

```
{
	"salespeople": {
		"id": 1,
		"first_name": "Thomas",
		"last_name": "Thompson",
		"employee_id": "TT123"
	}
}
```
</details>

<details>
<summary markdown="span">POST: Creating a salesperson requires a first name, last name and employee id. Returns all of the values from the request body, plus the id of the created object.
</summary>
JSON request body:

```
{
  "first_name": "Charlie",
  "last_name": "Brown",
  "employee_id": "CB123"
}
```

JSON return body:

```
{
	"id": 2,
	"first_name": "Charlie",
	"last_name": "Brown",
	"employee_id": "CB123"
}
```
</details>

| Action  | Method   | URL   |
|---|---|---|
| List all customers | GET  | http://localhost:8090/api/customers/  |
| Show a specific customer  | GET  |  http://localhost:8090/api/customers/:id/  |
| Create a customer  | POST   | http://localhost:8090/api/customers/ |
| Delete a customer  | DELETE  |  http://localhost:8090/api/customers/:id/  |

<details>
<summary markdown="span">GET (List): Returns an object with one key "customers" set to a list of all Customer objects.
</summary>
JSON return body:

```
{
	"customers": [
		{
			"id": 1,
			"first_name": "Jimmy",
			"last_name": "John",
			"address": "1234 Evergreen Drive, Seattle, WA",
			"phone_number": "123-456-7890"
		},
		{
			"id": 2,
			"first_name": "Daffy",
			"last_name": "Duck",
			"address": "1234 Story Lane, Seattle, WA",
			"phone_number": "234-567-8901"
		}
	]
}
```
</details>

<details>
<summary markdown="span">GET: Returns an object with one key "customer" set to the requested Customer object.
</summary>
JSON return body:

```
{
	"customer": {
		"id": 1,
		"first_name": "Jimmy",
		"last_name": "John",
		"address": "1234 Evergreen Drive, Seattle, WA",
		"phone_number": "123-456-7890"
	}
}
```
</details>

<details>
<summary markdown="span">POST: Creating a customer requires a first name, last name, address, and phone number. Returns all of the values from the request body, plus the href location and id of the created object.
</summary>
JSON request body:

```
{
	"first_name": "Daffy",
	"last_name": "Duck",
	"address": "1234 Story Lane, Seattle, WA",
	"phone_number": "234-567-8901"
}
```

JSON return body:

```
{
	"id": 2,
	"first_name": "Daffy",
	"last_name": "Duck",
	"address": "1234 Story Lane, Seattle, WA",
	"phone_number": "234-567-8901"
}
```
</details>

| Action  | Method   | URL   |
|---|---|---|
| List all sales | GET  | http://localhost:8090/api/sales/  |
| Show a specific sale  | GET  |  http://localhost:8090/api/sales/:id/  |
| Create a sale  | POST   | http://localhost:8090/api/sales/ |
| Delete a sale  | DELETE  |  http://localhost:8090/api/sales/:id/  |

<details>
<summary markdown="span">GET (List): Returns an object with one key "sales" set to a list of all Sale objects.
</summary>
JSON return body:

```
{
	"sales": [
		{
			"id": 1,
			"price": "111.11",
			"automobile": {
				"id": 1,
				"vin": "283926SJDLD11FF",
				"sold": true
			},
			"salesperson": {
				"id": 1,
				"first_name": "Thomas",
				"last_name": "Thompson",
				"employee_id": "DFS3AD88"
			},
			"customer": {
				"id": 3,
				"first_name": "Jonah",
				"last_name": "Hill",
				"address": "1234 Evergreen Drive, Seattle, WA",
				"phone_number": "9072537777"
			}
		},
		{
			"id": 2,
			"price": "222.22",
			"automobile": {
				"id": 2,
				"vin": "1C3CC5FB2AN120174",
				"sold": true
			},
			"salesperson": {
				"id": 1,
				"first_name": "Thomas",
				"last_name": "Thompson",
				"employee_id": "DFS3AD88"
			},
			"customer": {
				"id": 3,
				"first_name": "Jonah",
				"last_name": "Hill",
				"address": "1234 Evergreen Drive, Seattle, WA",
				"phone_number": "9072537777"
			}
		}
	]
}
```
</details>

<details>
<summary markdown="span">GET: Returns an object with one key "sale" set to the requested Sale object.
</summary>
JSON return body:

```
{
	"sale": {
		"id": 1,
		"price": "111.11",
		"automobile": {
			"id": 2,
			"vin": "283926SJDLD11FF"
		},
		"salesperson": {
			"id": 1,
			"first_name": "Thomas",
			"last_name": "Thompson",
			"employee_id": "DFS3AD88"
		},
		"customer": {
			"id": 3,
			"first_name": "Jonah",
			"last_name": "Hill",
			"address": "1234 Evergreen Drive, Seattle, WA",
			"phone_number": "9072537777"
		}
	}
}
```
</details>

<details>
<summary markdown="span">POST: Creating a sale requires a automobile vin, salesperson employee id, and customer phone number. Returns all of the corresponding objects, as well as the href and id of the Sale object.
</summary>
JSON request body:

```
{
	"price": 52222,
	"automobile": "1C3CC5FB2AN120174",
	"salesperson": "DFS3AD88",
	"customer": "9072537777"
}
```

JSON return body:

```
{
	"id": 2,
	"price": 52222,
	"automobile": {
		"id": 2,
		"sold": false,
		"vin": "1C3CC5FB2AN120174"
	},
	"salesperson": {
		"id": 1,
		"first_name": "Thomas",
		"last_name": "Thompson",
		"employee_id": "DFS3AD88"
	},
	"customer": {
		"id": 3,
		"first_name": "Jonah",
		"last_name": "Hill",
		"address": "1234 Evergreen Drive, Seattle, WA",
		"phone_number": "9072537777"
	}
}
```
</details>

| Action  | Method   | URL   |
|---|---|---|
| List all automobiles | GET  | http://localhost:8090/api/automobiles/  |
| Update (Sell) an automobile | PUT  | http://localhost:8090/api/automobiles/  |

<details>
<summary markdown="span">GET (List): Returns an object with one key "automobiles" set to a list of all AutomobileVO objects.
</summary>
JSON return body:

```
{
	"automobiles": [
		{
			"id": 2,
			"vin": "1C3CC5FB2AN120174",
			"sold": true
		},
		{
			"id": 21,
			"vin": "1likaushd",
			"sold": true
		}
	]
}
```
</details>

<details>
<summary markdown="span">PUT: Updating a AutomobileVO object to mark as "sold" as true only requires a vin. Returns corresponding AutomobileVO object.
</summary>
JSON request body:

```
{
	"id": 2,
	"vin": "1C3CC5FB2AN120174",
	"sold": true
}
```
</details>


# Service Microservice Models

The Service microservice has three models:

1. The `Appointment` model has fields for `date_time`, `reason`, `status`, `vin`, `customer`, and `technician`. Information for the appointments are stored with this model and it has a foreign key to the `Technician` model. This model has the capability to handle status changes for appointments (cancel, create, finish). The `vin` field stores the VIN number of every automobile in the inventory and also allows clients who did not buy the vehicle from the inventory to make appointments.

2. The `Technician` model contains fields for `first_name`, `last_name`, and `employee_id`, which is a primary key.

3. The `AutomobileVO` model is integrated with the **Inventory** microservice and has a unique `vin` field. A Poller is used to poll data from the `Automobile` model into the `AutomobileVO` model.

# Service Microservice RESTful API

| Action  | Method   | URL   |
|---------|----------|-------|
| List all VIN | GET  | http://localhost:8080/api/vin/ |

<details>
<summary markdown="span">GET (List): Returns an object with one key "autos" set to a list of all Autos objects.
</summary>
JSON return body:

```
{
	"autos": [
		{
			"id": 1,
			"vin": "1C3CC5FB2AN120174"
		}
	]
}
```
</details>

| Action  | Method   | URL   |
|---|---|---|
| List all technicians | GET  | http://localhost:8080/api/technicians/  |
| Create a technician  | POST   |  http://localhost:8080/api/technicians/ |
| Delete a technician  | DELETE  |  http://localhost:8080/api/technicians/:id/  |

<details>
<summary markdown="span">GET (List): Returns an object with one key "technicians" set to a list of all Techcnicians objects.
</summary>
JSON return body:

```
{
	"technicians": [
		{
			"id": 1,
			"first_name": "Karen",
			"last_name": "Knlee",
			"employee_id": 6
		}
	]
}
```
</details>

<details>
<summary markdown="span">POST: Creating a technician requires a first_name, last_name, employee_id. Returns all of the corresponding objects, as well as id of the Tehcnician object.
</summary>
JSON request body:

```
{
	"id": 1,
	"first_name": "Karen",
	"last_name": "Knlee",
	"employee_id": "6"
}
```
</details>

| Action  | Method   | URL   |
|---|---|---|
| List all appointments | GET  | http://localhost:8080/api/appointments/  |
| Create an appointment  | POST   | http://localhost:8080/api/appointments/ |
| Delete an appointment  | DELETE  |  http://localhost:8090/api/appointments/:id/  |
| Change appointment status to cancel  | PUT |  http://localhost:8080/api/appointments/:id/cancel/  |
| Change appointment status to finish  | PUT  |  http://localhost:8080/api/appointments/:id/finish/  |

<details>
<summary markdown="span">GET (List): Returns an object with one key "appointments" set to a list of all Appointment objects.
</summary>
JSON return body:

```
{
	"appointments": [
		{
			"id": 1,
			"date_time": "2023-04-07T21:35:00+00:00",
			"reason": "glass damage",
			"status": "created",
			"vin": "1C3CC5FB2AN120174",
			"customer": "John Wick",
			"technician": {
				"id": 1,
				"first_name": "Jhon",
				"last_name": "Doe",
				"employee_id": 2
			}
		}
  ]
}
```
</details>

<details>
<summary markdown="span">POST: Creating an appointment requires a date_time,reason,vin,customer,technician. Returns all of the corresponding objects, as well as id of the appointment object.
</summary>
JSON request body:

```
{
	"id": 11,
	"date_time": "2023-04-20T14:39:00.000Z",
	"reason": "broken engine",
	"status": "created",
	"vin": "123abc",
	"customer": "John Wick",
	"technician": {
		"id": 1,
		"first_name": "John",
		"last_name": "Doe",
		"employee_id": 2
	}
}
```
</details>

<details>
<summary markdown="span">PUT: Changing the status of the appointment to canceled. Returns all of the corresponding objects, as well as id of the appointment object with status changed to canceled.
</summary>
JSON request body:

```
{
	"id": 7,
	"date_time": "2023-04-20T14:39:00+00:00",
	"reason": "broken engine",
	"status": "canceled",
	"vin": "123abc",
	"customer": "John Wick",
	"technician": {
		"id": 1,
		"first_name": "Jogn",
		"last_name": "Doe",
		"employee_id": 2
	}
}
```
</details>

<details>
<summary markdown="span">PUT: Changing the status of the appointment to finished. Returns all of the corresponding objects, as well as id of the appointment object with status changed to finished.
</summary>
JSON request body:

```
{
	"id": 7,
	"date_time": "2023-04-20T14:39:00+00:00",
	"reason": "broken engine",
	"status": "canceled",
	"vin": "123abc",
	"customer": "John Wick",
	"technician": {
		"id": 1,
		"first_name": "John",
		"last_name": "Doe",
		"employee_id": 2
	}
}
```
</details>


## That's all folkS!
Happy coding!
