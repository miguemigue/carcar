import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CreateAutomobileForm from './CreateAutomobileForm';
import AutomobileList from './AutomobileList';
import CreateVehicleModel from './CreateModelForm';
import CreateSalesperson from './CreateSalesperson';
import SalespeopleList from './SalespeopleList';
import CreateCustomer from './CreateCustomerForm';
import CustomerList from './CustomersList';
import CreateSale from './CreateSaleForm';
import ListSales from './SalesList';
import AddTechnicianForm from './AddTechnicianForm';
import ListTechnicians from './ListTechnicians';
import AppointmentForm from './AppointmentForm'
import ListAppointments from './ListAppointments';
import ServiceHistoryList from './ServiceHistoryList';
import SalespersonHistory from './SalespersonHistory';
import ListManufacturers from './ListManufacturers';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelForm from './VehicleModelForm';
import VehicleModelList from './VehicleModelList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/automobiles/create/" element={<CreateAutomobileForm />} />
          <Route path="/automobiles/" element={<AutomobileList />} />
          <Route path="/models/create/" element={<CreateVehicleModel />} />
          <Route path="/salespeople/create/" element={<CreateSalesperson />} />
          <Route path="/salespeople/" element={<SalespeopleList />} />
          <Route path="/customers/create/" element={<CreateCustomer />} />
          <Route path="/customers/" element={<CustomerList />} />
          <Route path="/sales/create/" element={<CreateSale />} />
          <Route path="/sales/" element={<ListSales />} />
          <Route path="/technicians/create" element={<AddTechnicianForm />} />
          <Route path="/technicians/list" element={<ListTechnicians />} />
          <Route path="/appointments/create" element={<AppointmentForm />} />
          <Route path="/appointments/list" element={<ListAppointments />} />
          <Route path="/services/list" element={<ServiceHistoryList />} />
          <Route path="/manufacturers/list" element={<ListManufacturers />} />
          <Route path="/manufacturers/create" element={<ManufacturerForm />} />
          <Route path="/models/create" element={<VehicleModelForm />} />
          <Route path="/models/list" element={<VehicleModelList />} />
          <Route path="/sales/history/" element={<SalespersonHistory />} />



        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
