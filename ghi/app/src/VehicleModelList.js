import React, { useEffect, useState } from "react";
function VehicleModelList() {
  const [models, setModels] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const url = "http://localhost:8100/api/models/";
      try {
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setModels(data.models);
        }
      } catch (e) {
        console.error(e);
      }
    };
    fetchData();
  }, []);

  return (
    <div>
      <h1 className="mt-4 mb-3">Models</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models.map((model) => {
            return (
              <tr key={model.href}>
                <td>{model.name}</td>
                <td>{model.manufacturer.name}</td>
                <td>
                  <img
                    src={model.picture_url}
                    className="img-fluid shadow"
                    alt="..."
                    style={{ maxWidth: "300px", maxHeight: "300px" }}
                  />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default VehicleModelList;
