import React, { useEffect, useState } from 'react';
import AlertMessage from './common components/AlertMessage';


function CreateSale() {
    const [automobiles, setAutomobiles] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [automobile, setAutomobile] = useState('');
    const [salesperson, setSalesperson] = useState('');
    const [customer, setCustomer] = useState('');
    const [price, setPrice] = useState(0.00);
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const [showFailureMessage, setShowFailureMessage] = useState(false);

    const fetchData = async () => {
        const automobilesUrl = 'http://localhost:8090/api/automobiles/';
        const automobilesResponse = await fetch(automobilesUrl);
        if (automobilesResponse.ok) {
            const forSaleAutos = [];
            const allAutos = await automobilesResponse.json();
            for (let auto of allAutos.automobiles) {
                if (auto.sold === false) {
                    forSaleAutos.push(auto);
                }
            }
            setAutomobiles(forSaleAutos);
        }

        const salespeopleUrl = 'http://localhost:8090/api/salespeople/'
        const salespeopleResponse = await fetch(salespeopleUrl);
        if (salespeopleResponse.ok) {
            const data = await salespeopleResponse.json();
            setSalespeople(data.salespeople);
        }

        const customersUrl = 'http://localhost:8090/api/customers/'
        const customersResponse = await fetch(customersUrl);
        if (customersResponse.ok) {
          const data = await customersResponse.json();
          setCustomers(data.customers);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.automobile = automobile;
        data.salesperson = salesperson;
        data.customer = customer;
        data.price = price;

        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            const autoData = {};
            autoData.vin = automobile;
            const automobileUrl = 'http://localhost:8090/api/automobiles/';
            const fetchAutomobileConfig = {
                method: "put",
                body: JSON.stringify(autoData),
                headers: {
                    'Content-Type': 'application/json',
                }
            };
            const autoResponse = await fetch(automobileUrl, fetchAutomobileConfig);
            if (autoResponse.ok) {
                setAutomobile('');
                setSalesperson('');
                setCustomer('');
                setPrice('');
                // fetchData();
                setShowSuccessMessage(true);
                setShowFailureMessage(false);
            } else {
                setShowSuccessMessage(false);
                setShowFailureMessage(true);
            }
        }
    }


    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new sale</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="mb-3">
                            <label htmlFor="automobile">Automobile VIN</label>
                            <select onChange={handleAutomobileChange} required name="automobile" id="automobile" className="form-select">
                            <option value="">Choose an automobile VIN</option>
                            {automobiles.map(automobile => {
                                return (
                                    <option key={automobile.vin} value={automobile.vin}>
                                        {automobile.vin}
                                    </option>
                                );
                            })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="salesperson">Salesperson</label>
                            <select onChange={handleSalespersonChange} required name="salesperson" id="salesperson" className="form-select">
                            <option value="">Choose an salesperson</option>
                            {salespeople.map(salesperson => {
                                return (
                                    <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                        {`${salesperson.first_name} ${salesperson.last_name}`}
                                    </option>
                                );
                            })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="customer">Customer</label>
                            <select onChange={handleCustomerChange} required name="customer" id="customer" className="form-select">
                            <option value="">Choose an customer</option>
                            {customers.map(customer => {
                                return (
                                    <option key={customer.phone_number} value={customer.phone_number}>
                                        {`${customer.first_name} ${customer.last_name}`}
                                    </option>
                                );
                            })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="price">Price</label>
                            <input value={price} onChange={handlePriceChange} required type="text" name="price" id="price" className="form-control"/>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                    
                    {showSuccessMessage && <AlertMessage type="success" message="Sale successfully created!" />}
                    {showFailureMessage && <AlertMessage type="danger" message="Failed to create Sale because of existing VIN. Please try again." />}
                </div>
            </div>
        </div>
    );
}



export default CreateSale;
