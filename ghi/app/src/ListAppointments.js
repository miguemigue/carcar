import React, { useEffect, useState } from "react";

function ListAppointments() {
  const [appointments, setAppointments] = useState([]);

  const getDate = (dateString) => {
    let date = new Date(dateString);
    let formattedDate = date.toLocaleDateString("en-us", {
      year: "numeric",
      month: "numeric",
      day: "numeric",
    });
    return formattedDate;
  };

  const getTime = (timeString) => {
    let time = new Date(timeString);
    let formattedTime = time.toLocaleTimeString("en-us", {
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
      hour12: true,
    });
    return formattedTime;
  };

  const handlecancelAppointment = async (appointmentId) => {
    const url = `http://localhost:8080/api/appointments/${appointmentId}/cancel/`;
    try {
      const response = await fetch(url, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
      });

      if (response.ok) {
        appointments.map((appointment) => {
          if (appointment.id === appointmentId) {
            appointment.status = "canceled";
          }
        });
        setAppointments([...appointments]);
      } else {
        console.error("Failed to cancel appointment.");
      }
    } catch (e) {
      console.error(e);
    }
  };

  const handlefinishAppointment = async (appointmentId) => {
    const url = `http://localhost:8080/api/appointments/${appointmentId}/finish/`;
    try {
      const response = await fetch(url, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (response.ok) {
        appointments.map((appointment) => {
          if (appointment.id === appointmentId) {
            appointment.status = "finished";
          }
        });
        setAppointments([...appointments]);
      } else {
        console.error("Failed to mark appointment as finished.");
      }
    } catch (e) {
      console.error(e);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      const url = "http://localhost:8080/api/appointments/";
      const vinUrl = "http://localhost:8080/api/vin/";
      try {
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          const vinResponse = await fetch(vinUrl);
          if (vinResponse.ok) {
            const vinData = await vinResponse.json();
            let vipVins = [];
            vinData.autos.map((auto) => {
              vipVins.push(auto.vin);
            });
            data.appointments.map((appointment) => {
              if (vipVins.includes(appointment.vin)) {
                appointment.vip = true;
              }
            });
            setAppointments(data.appointments);
          }
        }
      } catch (e) {
        console.error(e);
      }
    };
    fetchData();
  }, []);

  return (
    <div>
      <h1 className="mt-4 mb-3">Service Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment) => {
            if (appointment.status === "created") {
              return (
                <tr key={appointment.id}>
                  <td>{appointment.vin}</td>
                  <td>{appointment.vip ? "YES" : "NO"}</td>
                  <td>{appointment.customer}</td>
                  <td>{getDate(appointment.date_time)}</td>
                  <td>{getTime(appointment.date_time)}</td>
                  <td>{appointment.technician.first_name}</td>
                  <td>{appointment.reason}</td>
                  <td>
                    <div className="d-flex justify-content-center justify-content-md-start">
                      <button
                        type="button"
                        onClick={() => handlecancelAppointment(appointment.id)}
                        className="btn btn-outline-danger me-md-2"
                      >
                        <i className="bi bi-x-lg"></i> Cancel
                      </button>
                      <button
                        type="button"
                        onClick={() => handlefinishAppointment(appointment.id)}
                        className="btn btn-outline-success"
                      >
                        <i className="bi bi-check-lg"></i> Finish
                      </button>
                    </div>
                  </td>
                </tr>
              );
            }
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ListAppointments;
