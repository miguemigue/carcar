import { useEffect, useState } from "react";
import AlertMessage from "./common components/AlertMessage";

function CreateAutomobileForm() {
  const [color, setColor] = useState("");
  const [year, setYear] = useState("");
  const [vin, setVin] = useState("");
  const [model, setModel] = useState("");
  const [models, setModels] = useState([]);
  const [showSuccessMessage, setShowSuccessMessage] = useState(false);
  const [showFailureMessage, setShowFailureMessage] = useState(false);

  const fetchData = async () => {
    const url = "http://localhost:8100/api/models/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    } else {
      console.error(response);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.color = color;
    data.year = year;
    data.vin = vin;
    data.model_id = model;

    const autoUrl = "http://localhost:8100/api/automobiles/";
    const fetchOptions = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(autoUrl, fetchOptions);
    if (response.ok) {
      setShowSuccessMessage(true);
      setShowFailureMessage(false);
      setColor("");
      setYear("");
      setVin("");
      setModel("");
    } else {
      setShowSuccessMessage(false);
      setShowFailureMessage(true);
    }
  };

  const handleChangeColor = (event) => {
    const value = event.target.value;
    setColor(value);
  };

  const handleChangeYear = (event) => {
    const value = event.target.value;
    setYear(value);
  };

  const handleChangeVin = (event) => {
    const value = event.target.value;
    setVin(value);
  };

  const handleChangeModel = (event) => {
    const value = event.target.value;
    setModel(value);
  };

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add an automobile to inventory</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  onChange={handleChangeColor}
                  value={color}
                  required
                  placeholder="Color"
                  type="text"
                  id="color"
                  name="color"
                  className="form-control"
                />
                <label htmlFor="color">Color...</label>
              </div>
              <div className="col">
                <div className="form-floating mb-3">
                  <input
                    onChange={handleChangeYear}
                    value={year}
                    required
                    placeholder="Year"
                    type="text"
                    id="year"
                    name="year"
                    className="form-control"
                  />
                  <label htmlFor="year">Year...</label>
                </div>
              </div>
              <div className="col">
                <div className="form-floating mb-3">
                  <input
                    onChange={handleChangeVin}
                    value={vin}
                    required
                    placeholder="VIN"
                    type="text"
                    id="vin"
                    name="vin"
                    className="form-control"
                  />
                  <label htmlFor="VIN...">VIN...</label>
                </div>
              </div>
              <div className="col">
                <div className="form-floating mb-3">
                  <select
                    onChange={handleChangeModel}
                    value={model}
                    required
                    placeholder="Choose a model"
                    type="text"
                    id="model"
                    name="model"
                    className="form-control"
                  >
                    <option value="">Choose a model...</option>
                    {models.map((model) => {
                      return (
                        <option key={model.id} value={model.id}>
                          {model.name}
                        </option>
                      );
                    })}
                  </select>
                </div>
              </div>
              <div className="form-floating mb-3">
                <button className="btn btn-primary">Add Automobile</button>
              </div>
            </form>
            {showSuccessMessage && (
              <AlertMessage
                type="success"
                message="Automobile successfully created"
              />
            )}
            {showFailureMessage && (
              <AlertMessage
                type="danger"
                message="Failed to creare an automobile"
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default CreateAutomobileForm;
