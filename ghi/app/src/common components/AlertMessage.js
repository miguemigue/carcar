import React, { useState, useEffect } from 'react';

const AlertMessage = ({ type, message }) => {
    const [isVisible, setIsVisible] = useState(true);

    useEffect(() => {
        // Hide the alert message after 3 seconds
        const timer = setTimeout(() => {
            setIsVisible(false);
        }, 3000);

        // Clean up the timer on component when done
        return () => clearTimeout(timer);
    }, []);

    return (
        isVisible && (
            <div className={`alert alert-${type} mt-3 text-center`} role="alert">
                {message}
            </div>
        )
    );
};

export default AlertMessage;
