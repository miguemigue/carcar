import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            {/* Manufacturers */}
            <li className="nav-item dropdown">
              <NavLink
                className="nav-link dropdown-toggle"
                to="/"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Manufacturers
              </NavLink>
              <ul className="dropdown-menu">
                <li>
                  <NavLink className="dropdown-item" to="/manufacturers/list">
                    All Manufacturers
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/manufacturers/create">
                    Create a Manufacturer
                  </NavLink>
                </li>
              </ul>
            </li>
            {/* Vehicle Models */}
            <li className="nav-item dropdown">
              <NavLink
                className="nav-link dropdown-toggle"
                to="/"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Models
              </NavLink>
              <ul className="dropdown-menu">
                <li>
                  <NavLink className="dropdown-item" to="/models/list">
                    All Models
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/models/create">
                    Create a Model
                  </NavLink>
                </li>
              </ul>
            </li>

            {/* Automobiles */}
            <li className="nav-item dropdown">
              <NavLink
                className="nav-link dropdown-toggle"
                to="/"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Automobiles
              </NavLink>
              <ul className="dropdown-menu">
                <li>
                  <NavLink className="dropdown-item" to="/automobiles/">
                    Automobiles List
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/automobiles/create/">
                    Add an Automobile
                  </NavLink>
                </li>
              </ul>
            </li>

            {/* Technicians */}
            <li className="nav-item dropdown">
              <NavLink
                className="nav-link dropdown-toggle"
                to="/"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Technicians
              </NavLink>
              <ul className="dropdown-menu">
                <li>
                  <NavLink className="dropdown-item" to="/technicians/list">
                    All Technicians
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/technicians/create">
                    Create a Technician
                  </NavLink>
                </li>
              </ul>
            </li>

            {/* Appointments */}
            <li className="nav-item dropdown">
              <NavLink
                className="nav-link dropdown-toggle"
                to="/"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Appointments
              </NavLink>
              <ul className="dropdown-menu">
                <li>
                  <NavLink className="dropdown-item" to="/appointments/list">
                    All Appointments
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/appointments/create">
                    Create a service appointment{" "}
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/services/list">
                    Service History{" "}
                  </NavLink>
                </li>
              </ul>
            </li>
            {/* Sales */}

            <li className="nav-item dropdown">
              <NavLink
                className="nav-link dropdown-toggle"
                to="/"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Automobile Sales
              </NavLink>
              <ul className="dropdown-menu">
                <li>
                  <NavLink className="dropdown-item" to="/salespeople/create/">
                    Add a Salesperson
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/salespeople/">
                    Salespeople
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/customers/create/">
                    Create a Customer
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/customers/">
                    Customers
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/sales/create/">
                    Add a Sale
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/sales/">
                    Sales
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/sales/history/">
                    Salesperson History
                  </NavLink>
                </li>
              </ul>
            </li>
          </ul>
          <NavLink to="/appointments/create" className="btn btn-warning me-3">
            Schedule a service
          </NavLink>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
