import { useEffect, useState } from "react";
import AlertMessage from "./common components/AlertMessage";

function CreateVehicleModel() {
  const [name, setName] = useState("");
  const [picture_url, setPictureUrl] = useState("");
  const [manufacturer, setManufacturer] = useState("");
  const [manufacturers, setManufacturers] = useState([]);
  const [showSuccessMessage, setShowSuccessMessage] = useState(false);
  const [showFailureMessage, setShowFailureMessage] = useState(false);

  const fetchData = async () => {
    const response = await fetch("http://localhost:8100/api/manufacturers/");
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    } else {
      console.error(response);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.name = name;
    data.picture_url = picture_url;
    data.manufacturer_id = manufacturer;

    const modelUrl = "http://localhost:8100/api/models/";
    const fetchOptions = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(modelUrl, fetchOptions);
    if (response.ok) {
      setShowSuccessMessage(true);
      setShowFailureMessage(false);
      setName("");
      setPictureUrl("");
      setManufacturer("");
    } else {
      setShowSuccessMessage(false);
      setShowFailureMessage(true);
    }
  };

  const handleChangeName = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handleChangePictureUrl = (event) => {
    const value = event.target.value;
    setPictureUrl(value);
  };

  const handleChangeManufacturer = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  };

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a vehicle model</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  onChange={handleChangeName}
                  value={name}
                  required
                  placeholder="Model name..."
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="Model name...">Model name...</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleChangePictureUrl}
                  value={picture_url}
                  required
                  placeholder="Picture URL..."
                  name="picture_url"
                  id="picture_url"
                  className="form-control"
                />
                <label htmlFor="Picture URL...">Picture URL...</label>
              </div>
              <div className="col">
                <div className="mb-3">
                  <select
                    onChange={handleChangeManufacturer}
                    value={manufacturer}
                    required
                    placeholder="Choose a manufacturer..."
                    type="text"
                    id="manufacturer"
                    name="manufacturer"
                    className="form-control"
                  >
                    <option value="">Choose a manufacturer...</option>
                    {manufacturers.map((manufacturer) => {
                      return (
                        <option key={manufacturer.id} value={manufacturer.id}>
                          {manufacturer.name}
                        </option>
                      );
                    })}
                  </select>
                </div>
              </div>
              <div>
                <button className="btn btn-primary">Create Vehicle</button>
              </div>
            </form>
            {showSuccessMessage && (
              <AlertMessage
                type="success"
                message="Model successfully created"
              />
            )}
            {showFailureMessage && (
              <AlertMessage type="danger" message="Failed to creare a model" />
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default CreateVehicleModel;
