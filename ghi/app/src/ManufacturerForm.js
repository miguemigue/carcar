import React, { useState, useEffect } from "react";
import AlertMessage from "./common components/AlertMessage";

function ManufacturerForm() {
  const [name, setName] = useState("");
  const [showSuccessMessage, setShowSuccessMessage] = useState(false);
  const [showFailureMessage, setShowFailureMessage] = useState(false);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;

    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
      setShowSuccessMessage(true);
      setShowFailureMessage(false);
      setName("");
    } else {
      setShowSuccessMessage(false);
      setShowFailureMessage(true);
    }
  };

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a manufacturer</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input
                value={name}
                onChange={handleNameChange}
                placeholder="Manufacturer name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="first_name">Manufacturer name</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
          {showSuccessMessage && (
            <AlertMessage
              type="success"
              message="Manufacturer successfully created!"
            />
          )}
          {showFailureMessage && (
            <AlertMessage
              type="danger"
              message="Failed to create Manufacturer. Please try again."
            />
          )}
        </div>
      </div>
    </div>
  );
}

export default ManufacturerForm;
