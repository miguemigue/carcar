import { useState, useEffect } from "react";
import AlertMessage from "./common components/AlertMessage";

function CreateCustomer() {
  const [first_name, setFirstName] = useState("");
  const [last_name, setLastName] = useState("");
  const [address, setAddress] = useState("");
  const [phone_number, setPhoneNumber] = useState("");
  const [showSuccessMessage, setShowSuccessMessage] = useState(false);
  const [showFailureMessage, setShowFailureMessage] = useState(false);
  const [customers, setCustomers] = useState([]);

  const handleChangeFirstName = (event) => {
    const value = event.target.value;
    setFirstName(value);
  };

  const handleChangeLastName = (event) => {
    const value = event.target.value;
    setLastName(value);
  };

  const handleChangeAddress = (event) => {
    const value = event.target.value;
    setAddress(value);
  };

  const handleChangePhoneNumber = (event) => {
    const value = event.target.value;
    setPhoneNumber(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.first_name = first_name;
    data.last_name = last_name;
    data.address = address;
    data.phone_number = phone_number;

    const url = "http://localhost:8090/api/customers/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: { "Content-Type": "application/json" },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const newCustomer = await response.json();
      setShowSuccessMessage(true);
      setShowFailureMessage(false);
      setFirstName("");
      setLastName("");
      setAddress("");
      setPhoneNumber("");
    }
  };

  const fetchData = async () => {
    const url = "http://localhost:8090/api/customers/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Customer</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  onChange={handleChangeFirstName}
                  value={first_name}
                  required
                  placeholder="First Name..."
                  type="text"
                  id="first_name"
                  name="first_name"
                  className="form-control"
                />
                <label htmlFor="first_name">First Name...</label>
              </div>
              <div className="col">
                <div className="form-floating mb-3">
                  <input
                    onChange={handleChangeLastName}
                    value={last_name}
                    required
                    placeholder="Last Name..."
                    type="text"
                    id="last_name"
                    name="last_name"
                    className="form-control"
                  />
                  <label htmlFor="last_name">Last Name...</label>
                </div>
              </div>
              <div className="col">
                <div className="form-floating mb-3">
                  <input
                    onChange={handleChangeAddress}
                    value={address}
                    required
                    placeholder="Address..."
                    type="text"
                    id="address"
                    name="adress"
                    className="form-control"
                  />
                  <label htmlFor="address">Address...</label>
                </div>
              </div>
              <div className="col">
                <div className="form-floating mb-3">
                  <input
                    onChange={handleChangePhoneNumber}
                    value={phone_number}
                    required
                    placeholder="Phone Number..."
                    type="text"
                    id="phone_number"
                    name="phone_number"
                    className="form-control"
                  />
                  <label htmlFor="Phone Number...">Phone Number...</label>
                </div>
              </div>
              <div className="form-floating mb-3">
                <button className="btn btn-primary">Add Customer</button>
              </div>
            </form>
            {showSuccessMessage && (
              <AlertMessage
                type="success"
                message="Customer successfully created"
              />
            )}
            {showFailureMessage && (
              <AlertMessage
                type="danger"
                message="Failed to creare a Customer"
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default CreateCustomer;
