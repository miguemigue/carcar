import { useState } from "react";
import AlertMessage from "./common components/AlertMessage";

function CreateSalesperson() {
  const [first_name, setFirstName] = useState("");
  const [last_name, setLastName] = useState("");
  const [employee_id, setEmployeeId] = useState("");
  const [showSuccessMessage, setShowSuccessMessage] = useState(false);
  const [showFailureMessage, setShowFailureMessage] = useState(false);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.first_name = first_name;
    data.last_name = last_name;
    data.employee_id = employee_id;

    const url = "http://localhost:8090/api/salespeople/";
    const fetchOptions = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchOptions);
    if (response.ok) {
      setShowSuccessMessage(true);
      setShowFailureMessage(false);
      setFirstName("");
      setLastName("");
      setEmployeeId("");
    } else {
      setShowSuccessMessage(false);
      setShowFailureMessage(true);
    }
  };

  const handleChangeFirstName = (event) => {
    const value = event.target.value;
    setFirstName(value);
  };

  const handleChangeLastName = (event) => {
    const value = event.target.value;
    setLastName(value);
  };

  const handleChangeEmployeeId = (event) => {
    const value = event.target.value;
    setEmployeeId(value);
  };

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Salesperson</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  onChange={handleChangeFirstName}
                  value={first_name}
                  required
                  placeholder="first_name"
                  type="text"
                  id="first_name"
                  name="first_name"
                  className="form-control"
                />
                <label htmlFor="first_name">First Name...</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleChangeLastName}
                  value={last_name}
                  required
                  placeholder="last_name"
                  type="text"
                  id="last_name"
                  name="last_name"
                  className="form-control"
                />
                <label htmlFor="last_name">Last Name...</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleChangeEmployeeId}
                  value={employee_id}
                  required
                  placeholder="employee_id"
                  type="text"
                  id="mployee_id"
                  name="employee_id"
                  className="form-control"
                />
                <label htmlFor="employee_id">Employee ID...</label>
              </div>
              <div className="form-floating mb-3">
                <button className="btn btn-primary">Add Salesperson</button>
              </div>
            </form>
            {showSuccessMessage && (
              <AlertMessage
                type="success"
                message="Salesperson successfully created"
              />
            )}
            {showFailureMessage && (
              <AlertMessage
                type="danger"
                message="Failed to creare a salesperson"
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default CreateSalesperson;
