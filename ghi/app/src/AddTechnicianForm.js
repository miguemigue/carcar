import React, { useState } from 'react';
import AlertMessage from './common components/AlertMessage';

const AddTechnicianForm = () => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeId, setEmployeeId] = useState('');
  const [showSuccessMessage, setShowSuccessMessage] = useState(false);
  const [showFailureMessage, setShowFailureMessage] = useState(false);

  const handleFirstNameChange = (event) => {
    setFirstName(event.target.value);
  };

  const handleLastNameChange = (event) => {
    setLastName(event.target.value);
  };

  const handleEmployeeIdChange = (event) => {
    setEmployeeId(event.target.value);
  };

    // Make an API call to the backend to create the technician
    const technicianUrl = 'http://localhost:8080/api/technicians/';

    const handleSubmit = async (event) => {
      event.preventDefault();

      // Prepare the data to be sent to the backend
      const data = {
        first_name: firstName,
        last_name: lastName,
        employee_id: employeeId,
      };

      const fetchConfig = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      };

      try {
        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
          setShowSuccessMessage(true);
          setShowFailureMessage(false);
          // Reset form fields after successful submission
          setFirstName('');
          setLastName('');
          setEmployeeId('');
          const responseData = await response.json();
        } else {
          // Handle non-successful response
          setShowSuccessMessage(false);
          setShowFailureMessage(true);
        }
      } catch (error) {
      }
    };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Technician</h1>
          <form onSubmit={handleSubmit} id="create-technician-form">

            <div className="form-floating mb-3">
              <input
                onChange={handleFirstNameChange}
                placeholder="First name"
                required
                type="text"
                name="firstName"
                id="firstName"
                className="form-control"
                value={firstName}
              />
              <label htmlFor="firstName">First Name</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleLastNameChange}
                placeholder="Last name"
                required
                type="text"
                name="lastName"
                id="lastName"
                className="form-control"
                value={lastName}
              />
              <label htmlFor="lastName">Last Name</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleEmployeeIdChange}
                placeholder="Employee ID"
                required
                type="text"
                name="employeeId"
                id="employeeId"
                className="form-control"
                value={employeeId}
              />
              <label htmlFor="employeeId">Employee ID</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
            {showSuccessMessage && <AlertMessage type="success" message="Technician successfully added!" />}
            {showFailureMessage && <AlertMessage type="danger" message="Failed to add technician" />}
        </div>
      </div>
    </div>
  );

};

export default AddTechnicianForm;
